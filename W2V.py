import pandas as pd
from pyvi import ViTokenizer, ViPosTagger  # thư viện NLP tiếng Việt
from tqdm import tqdm
import numpy as np
import gensim  # thư viện NLP
from sklearn.feature_extraction.text import TfidfVectorizer
import os
from gensim.models import KeyedVectors
from keras.preprocessing.text import Tokenizer
import pickle
from sklearn.metrics import classification_report
import re

# ======================================
# đọc file train, valid, test
# ======================================

Train = pd.read_csv('data/train_nor_811.csv', header=0, names=['ID', 'Emotion', 'Sentence'], encoding='utf-8')
Valid = pd.read_csv('data/valid_nor_811.csv', header=0, names=['ID', 'Emotion', 'Sentence'], encoding='utf-8')
Test = pd.read_csv('data/test_nor_811.csv', header=0, names=['ID', 'Emotion', 'Sentence'], encoding='utf-8')

# ======================================
#  Chia dữ liệu
# ======================================

x_train = Train['Sentence'].str.lower()
ytr = Train['Emotion']

x_valid = Valid['Sentence'].str.lower()
yv = Valid['Emotion']

x_test = Test['Sentence'].str.lower()
yte = Test['Emotion']


# ======================================
#  xây dựng hàm tiền xử lí dữ liệu
# ======================================
def KillEmoji(string):
    #   Remove các ký tự kéo dài: vd: đẹppppppp
    string = re.sub(r'([A-Z])\1+', lambda m: m.group(1).upper(), string, flags=re.IGNORECASE)
    #     viết thường
    string = string.lower()
    #     link
    string = re.sub('<.*?>', '', string).strip()
    string = re.sub('(\s)+', r'\1', string)
    #     xóa ký tự đặt biệt
    string = re.sub(r"[-()\\\"#/@;:<>{}`+=~|.!?,%/]", "", string)
    string = re.sub('\n', ' ', string)
    string = re.sub('--', '', string)
    string = re.sub('  ', ' ', string)
    string = re.sub('   ', ' ', string)
    string = re.sub('    ', ' ', string)
    #     xóa số
    string = re.sub(r"\d+", "number", string)
    #     xóa
    string = re.sub("added.*photo", "", string)
    string = re.sub("added.*photos", "", string)
    string = re.sub("is.*post", "", string)
    string = re.sub("Photos.*post", "", string)
    string = re.sub("from.*post", "", string)
    string = re.sub("shared.*group", "", string)
    string = re.sub("shared.*post", "", string)
    string = re.sub("shared.*video", "", string)
    string = re.sub("is.*motivated", "", string)
    string = re.sub("is.*with", "", string)

    replace_list = {
        # Quy các icon về 2 loại emoj:
        "👹": " tức giận ", "👻": " sợ ", "💃": " thích thú ", '🤙': ' thích thú ', '👍': ' thích thú ',
        "💄": "khác", "💎": "khác", "💩": "kinh tởm", "😕": "buồn chán", "😱": "tức giận", "😸": "thích thú",
        "😾": "tức giận", "🚫": "khác", "🤬": "tức giận", "🧚": "khác", "🧡": "thích thú", '🐶': ' khác ',
        '👎': ' buồn chán ', '😣': ' tức giận ', '✨': ' khác ', '❣': ' khác ', '☀': ' khác ',
        '♥': ' thích thú ', '🤩': ' thích thú ', 'like': ' thích thú ', '💌': ' thích thú ',
        '🤣': ' thích thú ', '🖤': ' thích thú ', '🤤': ' thích thú ', ':(': ' buồn chán ', '😢': ' buồn chán ',
        '❤': ' thích thú ', '😍': ' thích thú ', '😘': ' thích thú ', '😪': ' buồn chán ', '😊': ' thích thú ',
        '😁': ' thích thú ', '💖': ' thích thú ', '😟': ' buồn chán ', '😭': ' buồn chán ',
        '💯': ' thích thú ', '💗': ' thích thú ', '♡': ' thích thú ', '💜': ' thích thú ', '🤗': ' thích thú ',
        '^^': ' thích thú ', '😨': ' kinh tởm ', '☺': ' thích thú ', '💋': ' thích thú ', '👌': ' thích thú ',
        '😖': ' kinh tởm ', '😀': ' thích thú ', ':((': ' buồn chán ', '😡': ' tức giận ', '😠': ' tức giận ',
        '😒': ' buồn chán ', '🙂': ' thích thú ', '😏': ' kinh tởm ', '😝': ' thích thú ', '😄': ' thích thú ',
        '😙': ' thích thú ', '😤': ' tức giận ', '😎': ' thích thú ', '😆': ' thích thú ', '💚': ' thích thú ',
        '✌': ' thích thú ', '💕': ' thích thú ', '😞': ' buồn chán ', '😓': ' buồn chán ', '️🆗️': ' thích thú ',
        '😉': ' thích thú ', '😂': ' thích thú ', ':v': '  thích thú ', '=))': '  thích thú ', '😋': ' thích thú ',
        '💓': ' thích thú ', '😐': ' buồn chán ', ':3': ' thích thú ', '😫': ' buồn chán ', '😥': ' buồn chán ',
        '😃': ' thích thú ', '😬': 'sợ', '😌': ' buồn chán ', '💛': ' thích thú ', '🤝': ' thích thú ', '🎈': ' khác ',
        '😗': ' thích thú ', '🤔': ' kinh tởm ', '😑': ' tức giận ', '🔥': ' khác ', '🙏': ' thích thú ',
        '🆗': ' thích thú ', '😻': ' thích thú ', '💙': ' thích thú ', '💟': ' thích thú ',
        '😚': ' thích thú ', '❌': ' khác ', '👏': ' thích thú ', ';)': ' thích thú ', '<3': ' thích thú ',
        '🌝': ' thích thú ', '🌷': ' khác ', '🌸': ' thích thú ', '🌺': ' thích thú ',
        '🌼': ' khác ', '🍓': ' khác ', '🐅': ' khác ', '🐾': ' thích thú ', '👉': ' khác ',
        '💐': ' khác ', '💞': ' thích thú ', '💥': ' khác ', '💪': ' khác ',
        '💰': ' khác ', '😇': ' thích thú ', '😛': ' thích thú ', '😜': ' thích thú ',
        '🙃': ' thích thú ', '🤑': ' thích thú ', '🤪': ' thích thú ', '☹': ' buồn chán ', '💀': ' khác ',
        '😔': ' buồn chán ', '😧': ' buồn chán ', '😩': ' buồn chán ', '😰': ' buồn chán ', '😳': ' buồn chán ',
        '😵': ' bất ngờ ', '😶': ' buồn chán ', '🙁': ' buồn chán ', }

    for k, v in replace_list.items():
        string = string.replace(k, v)
    #     remove nốt những ký tự thừa thãi

    string = string.replace(u'"', u' ')
    string = string.replace(u'️', u'')
    string = string.replace('🏻', '')

    return string


# ======================================
# Tiền xử lý dữ liệu
# ======================================


for i in range(len(x_train)):
    x_train[i] = KillEmoji(x_train[i])

for i in range(len(x_test)):
    x_test[i] = KillEmoji(x_test[i])

for i in range(len(x_valid)):
    x_valid[i] = KillEmoji(x_valid[i])


# ======================================
#       Tf-Idf Vectors as Features
# ======================================


vocab_size = 20000

max_len = 1200

max_features = 20000 #equal to vocab_size

num_labels = len(ytr.unique())
batch_size = 32
nb_epoch = 20

nof_filters = 200
kernel_size = 16

hidden_dims = 512


embedding_path = "data/xuly/W2V_ner.vec"

embed_size = 300

tk = Tokenizer(num_words = max_features, lower = True)

tk.fit_on_texts(x_train)

x_train = tk.texts_to_sequences(x_train)

x_valid = tk.texts_to_sequences(x_valid)

x_test = tk.texts_to_sequences(x_test)

from sklearn.preprocessing import LabelBinarizer
from keras.preprocessing.sequence import pad_sequences
x_train = pad_sequences(x_train, maxlen = max_len)
x_valid = pad_sequences(x_valid, maxlen = max_len)
x_test= pad_sequences(x_test, maxlen = max_len)
encoder = LabelBinarizer()
encoder.fit(ytr)
y_train = encoder.transform(ytr)
y_valid = encoder.transform(yv)
y_test = encoder.transform(yte)

def get_coefs(word,*arr):
    return word, np.asarray(arr, dtype='float32')
embedding_index = dict(get_coefs(*o.strip().split(" ")) for o in open(embedding_path, encoding='utf-8'))

word_index = tk.word_index
nb_words = min(max_features, len(word_index)+1)
embedding_matrix = np.zeros((nb_words, embed_size))
for word, i in word_index.items():
    if i >= max_features: continue
    embedding_vector = embedding_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector
# print("======================================================")
# print(embedding_vector)
# print("======================================================")


from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

# tfidf_vect = TfidfVectorizer(analyzer='word', max_features=30000)
# tfidf_vect.fit(x_train)  # learn vocabulary and idf from training set
#
# pickle.dump(tfidf_vect, open('tfidf_vect.pkl', 'wb'))

# X_train_tfidf = tfidf_vect.transform(x_train)
# X_valid_tfidf = tfidf_vect.transform(x_valid)
# X_test_tfidf = tfidf_vect.transform(x_test)

# ======================================
#           label encode
# ======================================

# from sklearn import preprocessing
#
# encoder = preprocessing.LabelEncoder()
# y_train = encoder.fit_transform(ytr)
# y_valid = encoder.fit_transform(yv).reshape(-1, 1)
# y_test = encoder.fit_transform(yte)

# pickle.dump(encoder, open('label_encode.pkl', 'wb'))

# ======================================
#       Xây dựng hàm Train
# ======================================
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score


def train_model(classifier, x_train, y_train, x_valid, y_valid, x_test, y_test, is_neuralnet=False, n_epochs=10):
    if is_neuralnet:
        classifier.fit(x_train, y_train, validation_data=(x_valid, y_valid), epochs=n_epochs, batch_size=32)

        val_predictions = classifier.predict(x_valid)
        test_predictions = classifier.predict(x_test)
        val_predictions = val_predictions.argmax(axis=-1)
        test_predictions = test_predictions.argmax(axis=-1)


    else:
        classifier.fit(x_train, y_train)
        model_name = type(classifier).__name__ + ".pkl"
        pickle.dump(classifier, open(model_name, 'wb'))

        val_predictions = classifier.predict(x_valid)
        test_predictions = classifier.predict(x_test)

    # print("Validation accuracy: ", accuracy_score(val_predictions, y_valid))
    # print("F1_Score Valid: ", f1_score(val_predictions, y_valid, average="macro"))
    # print("F1_Score test: ", f1_score(test_predictions, y_test, average="macro"))
    print(classification_report(y_test, test_predictions))
    return classifier


# ======================================
# Train với các thuật toán Deep Learning
# ======================================

#  --------LSTM---------
from keras.layers import Input, Reshape, Dense, LSTM, Bidirectional, GRU, Convolution1D, Flatten
from keras.optimizers import Adam
from keras.models import Model
print(x_train[5547])
def create_lstm_model():
    input_layer = Input(shape=(1200,))

    layer = Reshape((5548, 1200))(input_layer)
    layer = LSTM(128, activation='relu')(layer)
    layer = Dense(512, activation='relu')(layer)
    layer = Dense(512, activation='relu')(layer)
    layer = Dense(128, activation='relu')(layer)

    output_layer = Dense(10, activation='sigmoid')(layer)

    classifier = Model(input_layer, output_layer)
    classifier.compile(optimizer=Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return classifier


print("=============================")
print("LSTM Model")
name = train_model(create_lstm_model(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)
name.save("LSTM.h5")


# ----------RNN---------------
def create_brnn_model():
    input_layer = Input(shape=(1200,))

    layer = Reshape((7, 1206))(input_layer)
    layer = Bidirectional(GRU(128, activation='sigmoid'))(layer)
    layer = Dense(512, activation='relu')(layer)
    layer = Dense(512, activation='relu')(layer)
    layer = Dense(128, activation='relu')(layer)

    output_layer = Dense(10, activation='softmax')(layer)

    classifier = Model(input_layer, output_layer)

    classifier.compile(optimizer=Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    return classifier


print("=============================")
print("RNN Model")
name = train_model(create_brnn_model(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)
name.save("RNN.h5")


# -----------CNN------------
def create_rcnn_model():
    input_layer = Input(shape=(1200,))

    layer = Reshape((7, 1206))(input_layer)
    layer = Bidirectional(GRU(128, activation='relu', return_sequences=True))(layer)
    layer = Convolution1D(1206, 7, activation="relu")(layer)
    layer = Flatten()(layer)
    layer = Dense(512, activation='relu')(layer)
    layer = Dense(512, activation='relu')(layer)
    layer = Dense(128, activation='relu')(layer)

    output_layer = Dense(10, activation='sigmoid')(layer)

    classifier = Model(input_layer, output_layer)
    classifier.summary()
    classifier.compile(optimizer=Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    return classifier


print("=============================")
print("CNN Model")
name = train_model(create_rcnn_model(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)
name.save("CNN.h5")

# =============================
# Train với các thuật toán ML
# =============================

from sklearn.naive_bayes import ComplementNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier

# -----------Linear Classifier------------
print("=============================")
print("Linear Classifier")
train_model(LogisticRegression(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)

# -----------Naive Bayes------------
print("=============================")
print("Naive Bayes")
train_model(ComplementNB(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)
# -----------SVM------------
print("=============================")
print("SVM")
train_model(LinearSVC(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)
# -----------Bagging Model------------
print("=============================")
print("Bagging Model")
train_model(RandomForestClassifier(), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)

# -----------Decision Tree------------
print("=============================")
print("Decision Tree")
train_model(DecisionTreeClassifier(criterion='entropy'), x_train, y_train, x_valid, y_valid, x_test, y_test,
                   is_neuralnet=True)