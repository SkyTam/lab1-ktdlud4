from pyvi import ViTokenizer, ViPosTagger # thư viện NLP tiếng Việt
from gensim.models import KeyedVectors 
import gensim # thư viện NLP
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import pickle


# =============================
# tiền xử lí câu và chuyển sáng dạng vector
# =============================
def pretreatment(x_input):
    x_data = []
    x1_data = []
    # tiền xử lí
    lines = gensim.utils.simple_preprocess(x_input)
    lines = ' '.join(lines)
    lines = ViTokenizer.tokenize(lines)
    x_data.append(lines)
    x1_data.append(x_data)
    # chuyển đoạn test về dạng vector
    tfidf_vect = pickle.load(open('tfidf_vect.pkl', 'rb'))
    lines_tfidf =  tfidf_vect.transform(x_data)

    return lines_tfidf

# =============================
# encode label 
# =============================
def encode(input):
    from sklearn import preprocessing
    encoder = pickle.load(open('label_encode.pkl', 'rb'))
    x = []
    x.append(input)
    result = encoder.inverse_transform(x)
    return result


# LOAD CÁC THUẬT TOÁN ĐÃ TRAIN
DecisionTreeClassifier = pickle.load(open('DecisionTreeClassifier.pkl', 'rb'))
LogisticRegression = pickle.load(open('LogisticRegression.pkl', 'rb'))
MultinomialNB = pickle.load(open('MultinomialNB.pkl', 'rb'))
RandomForestClassifier = pickle.load(open('RandomForestClassifier.pkl', 'rb'))
SVC = pickle.load(open('SVC.pkl', 'rb'))

from keras.models import load_model
LSTM = load_model('LSTM.h5')
CNN = load_model('CNN.h5')
RNN = load_model('RNN.h5')


# TEST
print("nhập 1 câu:")
x_test = input()
x_test = pretreatment(x_test)

while (True):
    print("==================================")
    print("\t\t KẾT QUẢ")
    print("==================================")


    # KẾT QUẢ DỰ ĐOÁN
    print("========DECISION TREE==============")
    test_predictions = DecisionTreeClassifier.predict(x_test)
    print(encode(test_predictions))

    print("=========Linear Classifier=============")
    test_predictions = LogisticRegression.predict(x_test)
    print(encode(test_predictions))


    print("=========NAIVE BAYES=============")
    test_predictions = MultinomialNB.predict(x_test)
    print(encode(test_predictions))

    print("=========RandomForest=============")
    test_predictions = RandomForestClassifier.predict(x_test)
    print(encode(test_predictions))


    print("=========SVM=============")
    test_predictions = SVC.predict(x_test)
    print(encode(test_predictions))

    print("==========LMST============")
    test_predictions = LSTM.predict(x_test).argmax(axis=-1)
    print(encode(test_predictions))

    print("===========CNN===========")
    test_predictions = CNN.predict(x_test).argmax(axis=-1)
    print(encode(test_predictions))

    print("==========RNN============")
    test_predictions = RNN.predict(x_test).argmax(axis=-1)
    print(encode(test_predictions))

    print("nhập 1 câu:")
    x_test = input()
    if (x_test == "0"):
        exit()
    x_test = pretreatment(x_test)
    