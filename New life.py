from keras.models import Sequential
from keras.layers import Conv1D, GlobalMaxPooling1D, Embedding, LSTM
from keras.layers.core import Dense, Dropout, Activation
from keras.preprocessing.text import Tokenizer
from keras import metrics, regularizers
from keras.preprocessing import sequence
from keras.preprocessing.sequence import pad_sequences
import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer

#Load cleaned dataset

Train = pd.read_csv('data/train_nor_811.csv', header = 0, names = ['ID', 'Emotion', 'Sentence'])
Valid = pd.read_csv('data/valid_nor_811.csv', header = 0, names = ['ID', 'Emotion', 'Sentence'])
Test = pd.read_csv('data/test_nor_811.csv', header = 0, names = ['ID', 'Emotion', 'Sentence'])

train_sent = Train['Sentence'].str.lower()
train_emo = Train['Emotion']

valid_sent = Valid['Sentence'].str.lower()
valid_emo = Valid['Emotion']

test_sent = Test['Sentence'].str.lower()
test_emo = Test['Emotion']

print("----------------------BEFORE PREPROCESSING -----------------------")
for i in valid_sent:
    print("Sentence :", i)

import re


# preprocessing data
def Preprocess(string):
    #   Remove các ký tự kéo dài: vd: đẹppppppp
    string = re.sub(r'([A-Z])\1+', lambda m: m.group(1).upper(), string, flags=re.IGNORECASE)
    #     viết thường
    string = string.lower()
    #     link
    string = re.sub('<.*?>', '', string).strip()
    string = re.sub('(\s)+', r'\1', string)
    #     xóa ký tự đặt biệt
    string = re.sub(r"[-()\\\"#/@;:<>{}`+=~|.!?,%/]", "", string)
    string = re.sub('\n', ' ', string)
    string = re.sub('--', '', string)
    string = re.sub('  ', ' ', string)
    string = re.sub('   ', ' ', string)
    string = re.sub('    ', ' ', string)
    #     xóa số
    string = re.sub(r"\d+", "number", string)
    #     xóa
    string = re.sub("added.*photo", "", string)
    string = re.sub("added.*photos", "", string)
    string = re.sub("is.*post", "", string)
    string = re.sub("Photos.*post", "", string)
    string = re.sub("from.*post", "", string)
    string = re.sub("shared.*group", "", string)
    string = re.sub("shared.*post", "", string)
    string = re.sub("shared.*video", "", string)
    string = re.sub("is.*motivated", "", string)
    string = re.sub("is.*with", "", string)
    # emoji_pattern = re.compile("["
    #                        u"\U0001F600-\U0001F64F"  # emoticons
    #                        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
    #                        u"\U0001F680-\U0001F6FF"  # transport & map symbols
    #                        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
    #                        u"\U00002702-\U000027B0"
    #                        u"\U000024C2-\U0001F251"
    #                        "]+", flags=re.UNICODE)
    # string = emoji_pattern.sub(r'', string)
    # xử lý emoj
    replace_list = {
        # Quy các icon về 2 loại emoj:
        "👹": " tức giận ", "👻": " sợ ", "💃": " thích thú ", '🤙': ' thích thú ', '👍': ' thích thú ',
        "💄": "khác", "💎": "khác", "💩": "kinh tởm", "😕": "buồn chán", "😱": "tức giận", "😸": "thích thú",
        "😾": "tức giận", "🚫": "khác", "🤬": "tức giận", "🧚": "khác", "🧡": "thích thú", '🐶': ' khác ',
        '👎': ' buồn chán ', '😣': ' tức giận ', '✨': ' khác ', '❣': ' khác ', '☀': ' khác ',
        '♥': ' thích thú ', '🤩': ' thích thú ', 'like': ' thích thú ', '💌': ' thích thú ',
        '🤣': ' thích thú ', '🖤': ' thích thú ', '🤤': ' thích thú ', ':(': ' buồn chán ', '😢': ' buồn chán ',
        '❤': ' thích thú ', '😍': ' thích thú ', '😘': ' thích thú ', '😪': ' buồn chán ', '😊': ' thích thú ',
        '😁': ' thích thú ', '💖': ' thích thú ', '😟': ' buồn chán ', '😭': ' buồn chán ',
        '💯': ' thích thú ', '💗': ' thích thú ', '♡': ' thích thú ', '💜': ' thích thú ', '🤗': ' thích thú ',
        '^^': ' thích thú ', '😨': ' kinh tởm ', '☺': ' thích thú ', '💋': ' thích thú ', '👌': ' thích thú ',
        '😖': ' kinh tởm ', '😀': ' thích thú ', ':((': ' buồn chán ', '😡': ' tức giận ', '😠': ' tức giận ',
        '😒': ' buồn chán ', '🙂': ' thích thú ', '😏': ' kinh tởm ', '😝': ' thích thú ', '😄': ' thích thú ',
        '😙': ' thích thú ', '😤': ' tức giận ', '😎': ' thích thú ', '😆': ' thích thú ', '💚': ' thích thú ',
        '✌': ' thích thú ', '💕': ' thích thú ', '😞': ' buồn chán ', '😓': ' buồn chán ', '️🆗️': ' thích thú ',
        '😉': ' thích thú ', '😂': ' thích thú ', ':v': '  thích thú ', '=))': '  thích thú ', '😋': ' thích thú ',
        '💓': ' thích thú ', '😐': ' buồn chán ', ':3': ' thích thú ', '😫': ' buồn chán ', '😥': ' buồn chán ',
        '😃': ' thích thú ', '😬': 'sợ', '😌': ' buồn chán ', '💛': ' thích thú ', '🤝': ' thích thú ', '🎈': ' khác ',
        '😗': ' thích thú ', '🤔': ' kinh tởm ', '😑': ' tức giận ', '🔥': ' khác ', '🙏': ' thích thú ',
        '🆗': ' thích thú ', '😻': ' thích thú ', '💙': ' thích thú ', '💟': ' thích thú ',
        '😚': ' thích thú ', '❌': ' khác ', '👏': ' thích thú ', ';)': ' thích thú ', '<3': ' thích thú ',
        '🌝': ' thích thú ', '🌷': ' khác ', '🌸': ' thích thú ', '🌺': ' thích thú ',
        '🌼': ' khác ', '🍓': ' khác ', '🐅': ' khác ', '🐾': ' thích thú ', '👉': ' khác ',
        '💐': ' khác ', '💞': ' thích thú ', '💥': ' khác ', '💪': ' khác ',
        '💰': ' khác ', '😇': ' thích thú ', '😛': ' thích thú ', '😜': ' thích thú ',
        '🙃': ' thích thú ', '🤑': ' thích thú ', '🤪': ' thích thú ', '☹': ' buồn chán ', '💀': ' khác ',
        '😔': ' buồn chán ', '😧': ' buồn chán ', '😩': ' buồn chán ', '😰': ' buồn chán ', '😳': ' buồn chán ',
        '😵': ' bất ngờ ', '😶': ' buồn chán ', '🙁': ' buồn chán ', }

    for k, v in replace_list.items():
        string = string.replace(k, v)
    #     remove nốt những ký tự thừa thãi

    string = string.replace(u'"', u' ')
    string = string.replace(u'️', u'')
    string = string.replace('🏻', '')

    return string


for i in range(len(train_sent)):
    train_sent.values[i] = Preprocess(train_sent.values[i])

for i in range(len(valid_sent)):
    valid_sent.values[i] = Preprocess(valid_sent.values[i])

for i in range(len(test_sent)):
    test_sent.values[i] = Preprocess(test_sent.values[i])
print(valid_sent)
print("----------------------AFTER PREPROCESSING  1-----------------------")
for i in valid_sent:
    print("Sentence :", i)

dictionary = {}
with open("data/xuly/tiengvietchuan.txt", "r", encoding='utf-8') as file:
    for line in file:
        key, value = line.strip().split(",")
        key1 = str(key).strip()
        value1 = str(value).strip()
        dictionary[key1] = value1

print(dictionary)


def replace_string(text, dictionary):
    l = text.split(" ")
    for key in dictionary.keys():
        for item, va in enumerate(l):
            if va == key:
                l[item] = str(dictionary[key])
    result = " ".join(l)
    return result.strip()


for i in range(len(train_sent)):
    train_sent.values[i] = replace_string(train_sent.values[i], dictionary)

for i in range(len(valid_sent)):
    valid_sent.values[i] = replace_string(valid_sent.values[i], dictionary)

for i in range(len(test_sent)):
    test_sent.values[i] = replace_string(test_sent.values[i], dictionary)

print("----------------------AFTER PREPROCESSING  2-----------------------")
for i in valid_sent:
    print("Sentence :", i)

# Model Parameters
vocab_size = 20000

max_len = 1200

max_features = 20000 #equal to vocab_size

num_labels = len(train_emo.unique())
batch_size = 32
nb_epoch = 20

nof_filters = 200
kernel_size = 16

hidden_dims = 512


embedding_path = "data/xuly/W2V_ner.vec"

embed_size = 300

tk = Tokenizer(num_words = max_features, lower = True)

tk.fit_on_texts(train_sent)

x_train = tk.texts_to_sequences(train_sent)

x_valid = tk.texts_to_sequences(valid_sent)

x_test = tk.texts_to_sequences(test_sent)

from sklearn.preprocessing import LabelBinarizer
from keras.preprocessing.sequence import pad_sequences
x_train = pad_sequences(x_train, maxlen = max_len)
x_valid = pad_sequences(x_valid, maxlen = max_len)
x_test= pad_sequences(x_test, maxlen = max_len)

encoder = LabelBinarizer()
encoder.fit(train_emo)
Y_train = encoder.transform(train_emo)
Y_valid = encoder.transform(valid_emo)
Y_test = encoder.transform(test_emo)

def get_coefs(word,*arr):
    return word, np.asarray(arr, dtype='float32')
embedding_index = dict(get_coefs(*o.strip().split(" ")) for o in open(embedding_path, encoding='utf-8'))

word_index = tk.word_index
nb_words = min(max_features, len(word_index)+1)
embedding_matrix = np.zeros((nb_words, embed_size))
for word, i in word_index.items():
    if i >= max_features: continue
    embedding_vector = embedding_index.get(word)
    if embedding_vector is not None: embedding_matrix[i] = embedding_vector
print("======================================================")
print(embedding_vector)
print("======================================================")

model = Sequential()
model.add(Embedding(3870, embed_size,weights=[embedding_matrix], input_length = 1200, trainable=False))
#model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))

model.add(Conv1D(nof_filters, kernel_size, padding='valid', activation='relu', strides = 1))
model.add(GlobalMaxPooling1D())

model.add(Dense(hidden_dims))
model.add(Dropout(0.2))
model.add(Activation('relu'))

model.add(Dense(num_labels))

model.add(Activation('softmax'))

model.summary()
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics = [metrics.categorical_accuracy])

history = model.fit(x_train, Y_train,
                    batch_size = batch_size,
                    epochs = nb_epoch,
                    verbose = True,
                    validation_data = (x_valid, Y_valid))

score = model.evaluate(x_valid, Y_valid, batch_size=batch_size, verbose=True)

print('\nTest categorical_crossentropy:', score[0])
print('Categorical accuracy:', score[1])

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
print('--------kiem tra lai tren tap Test------------')
pred = model.predict(x_test, batch_size = 64, verbose = 1)
y_labelpred = pred.argmax(axis=-1)
y_labeltrue=Y_test.argmax(axis=-1)
print('accuracy',accuracy_score(y_labeltrue,y_labelpred))
print('micro_F1: ',f1_score(y_labeltrue,y_labelpred, average='micro'))
print('macro_F1: ',f1_score(y_labeltrue,y_labelpred, average='macro'))

print('--------kiem tra lai tren tap Valid------------')
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
predValid = model.predict(x_valid, batch_size = 64, verbose = 1)
y_labelpredValid = predValid.argmax(axis=-1)
y_labeltrueValid=Y_valid.argmax(axis=-1)
print('accuracy',accuracy_score(y_labelpredValid,y_labeltrueValid))
print('micro_F1: ',f1_score(y_labelpredValid,y_labeltrueValid, average='micro'))
print('macro_F1: ',f1_score(y_labelpredValid,y_labeltrueValid, average='macro'))

