#-----------Chia data---------------
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import pandas as pd
dataset = pd.read_csv(r"./data/dataset.csv",encoding='utf-8')
x = dataset.iloc[:,1:-1]
y = dataset.iloc[:,-1]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
#----------------------Chuong--------------------------
tree = DecisionTreeClassifier(criterion = 'entropy').fit(x_train, y_train)
y_pred = tree.predict(x_test)
print('Accuracy của Decision Tree là:', accuracy_score(y_test, y_pred)*100,"%") 
print(classification_report(y_test, y_pred))
print(confusion_matrix(y_test, y_pred))
#----------------------luu file train test--------------------------
df=pd.DataFrame(x_train)
df.to_csv('trainfile.csv',index=False, encoding='utf-8')

df=pd.DataFrame(x_test)
df.to_csv('testfile.csv',index=False, encoding='utf-8')
#---------------------Nhung------------------------------
from sklearn.naive_bayes import GaussianNB
classifier = GaussianNB()
classifier.fit(x_train, y_train)
y_pred = classifier.predict(x_test)
print('Accuracy của Naive Bayes là:', accuracy_score(y_test, y_pred)*100,"%") 
print(classification_report(y_test, y_pred))
print(confusion_matrix(y_test, y_pred))
#------------------------Yen----------------------------------
from sklearn.ensemble import RandomForestClassifier
clf=RandomForestClassifier(n_estimators=100)
clf.fit(x_train,y_train)
y_pred=clf.predict(x_test)
print("Accuracy của Random Forest: ",accuracy_score(y_test, y_pred)*100,"%" )
print(classification_report(y_test, y_pred))
print(confusion_matrix(y_test, y_pred))

#-------------------------------Tuan-----------------------------------------
from sklearn.svm import SVC
#clf = SVC()
#clf.fit(x_train, y_train)
#y_pred=clf.predict(x_test)
svm_model_linear = SVC(kernel = 'linear', C = 1).fit(x_train, y_train) 
y_pred = svm_model_linear.predict(x_test)
print("Accuracy của SVM: ",accuracy_score(y_test, y_pred)*100,"%" )
print(classification_report(y_test, y_pred))
print(confusion_matrix(y_test, y_pred))

#----------------------------Hien---------------------------------------
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
scaler = StandardScaler()
scaler.fit(x_train)
X_train = scaler.transform(x_train)
X_test = scaler.transform(x_test)
classifier = KNeighborsClassifier(n_neighbors=20)
classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)
print('Accuracy của kNN là:', accuracy_score(y_test, y_pred)*100,"%")
print(classification_report(y_test, y_pred))
print(confusion_matrix(y_test, y_pred))
