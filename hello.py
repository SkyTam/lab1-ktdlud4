import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from pyvi import ViTokenizer, ViPosTagger  # thư viện NLP tiếng Việt
from tqdm import tqdm
import numpy as np
import gensim  # thư viện NLP
import os
from gensim.models import KeyedVectors, word2vec


# hàm tiền xử lí dữ liệu
def pretreatment(x_input, y_input):
    x_data = []

    for lines in x_input:
        lines = gensim.utils.simple_preprocess(lines)
        lines = ' '.join(lines)
        lines = ViTokenizer.tokenize(lines)
        x_data.append(lines)
    return x_data, y_input


# đọc file train, valid, test
df_train = pd.read_csv(r'train_nor_811.csv', encoding='utf-8')
df_valid = pd.read_csv(r'valid_nor_811.csv',encoding='utf-8')
df_test = pd.read_csv(r'test_nor_811.csv', encoding='utf-8')

#xử lý biểu tượng cảm : xóa các ký tự đặc biệt

#  tiền xử lí file train, valid, test
x_train, y_train = pretreatment(df_train.Sentence.tolist(), df_train.Emotion.tolist())
x_valid, y_valid = pretreatment(df_valid.Sentence.tolist(), df_valid.Emotion.tolist())
x_test, y_test = pretreatment(df_test.Sentence.tolist(), df_test.Emotion.tolist())


#xử lý stop words
data1 = open('vietnamese-stopwords.txt', 'r', encoding='utf-8')
data = data1.read()
def stopword(x_test):
    data2 = []
    for line in x_test:
        item1 = ''
        for item in line.split():
            if item not in data:
                item1 = str(item1) + str(item) + " "
        if item1 == '':
            continue
        else:
            data2.append(item1)

    return data2

stopword_xtrain = stopword(x_train)
stopword_xtest = stopword(x_test)
stopword_xvalid = stopword(x_valid)

# #BoW
# def Tudien(bow):
#     Tudien = {}
#     for num in range(len(bow)):
#         bowA = bow[num].split(" ")
#         Tudien = set(bowA).union(set(Tudien))
#     return Tudien
# def BoW(bow):
#     a = Tudien(bow)
#     w = []
#     for line in bow:
#         w1 = dict.fromkeys(a, 0)
#         bowA = line.split(" ")
#         for tu in bowA:
#             w1[tu] += 1
#         w.append(w1)
#     # w.pop("")
#     return w
# w = BoW(stopword_xtrain)
# q = BoW(stopword_xvalid)
# # print(w)




def cmn(x):
    tfidf_vect = TfidfVectorizer(analyzer='word', max_features=30000)
    tfidf_vect.fit(x)  # learn vocabulary and idf from training set
    Tudien = tfidf_vect.get_feature_names()
    TFIDF = tfidf_vect.transform(x)
    print(Tudien)
    print(TFIDF.shape)
    return TFIDF
print(cmn(stopword_xtrain))