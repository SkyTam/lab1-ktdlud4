from tkinter import *
from tkinter import filedialog
# from tkinter import messagebox
from scipy.stats import zscore
import tkinter.ttk as cm
from pyvi import ViTokenizer
import pickle
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import pandas as pd
import gensim
import tkinter.scrolledtext as sc
import math
import csv


from keras.models import load_model
LSTM = load_model('LSTM.h5')
CNN = load_model('CNN.h5')
RNN = load_model('RNN.h5')
class Giaodien(Frame):
    def RunMain(self):
        self.update()
        string = self.Cau.get()
        self.NameTT['text'] = "Câu của bạn là: " + string
        string = re.sub(r'([A-Z])\1+', lambda m: m.group(1).upper(), string, flags=re.IGNORECASE)
        #     viết thường
        string = string.lower()
        #     link
        string = re.sub('<.*?>', '', string).strip()
        string = re.sub('(\s)+', r'\1', string)
        #     xóa ký tự đặt biệt
        string = re.sub(r"[-()\\\"#/@;:<>{}`+=~|.!?,%/]", "", string)
        string = re.sub('\n', ' ', string)
        string = re.sub('--', '', string)
        string = re.sub('  ', ' ', string)
        string = re.sub('   ', ' ', string)
        string = re.sub('    ', ' ', string)
        #     xóa số
        string = re.sub(r"\d+", "number", string)
        #     xóa
        string = re.sub("added.*photo", "", string)
        string = re.sub("added.*photos", "", string)
        string = re.sub("is.*post", "", string)
        string = re.sub("Photos.*post", "", string)
        string = re.sub("from.*post", "", string)
        string = re.sub("shared.*group", "", string)
        string = re.sub("shared.*post", "", string)
        string = re.sub("shared.*video", "", string)
        string = re.sub("is.*motivated", "", string)
        string = re.sub("is.*with", "", string)

        replace_list = {
            # Quy các icon về 2 loại emoj:
            "👹": " tức giận ", "👻": " sợ ", "💃": " thích thú ", '🤙': ' thích thú ', '👍': ' thích thú ',
            "💄": "khác", "💎": "khác", "💩": "kinh tởm", "😕": "buồn chán", "😱": "tức giận", "😸": "thích thú",
            "😾": "tức giận", "🚫": "khác", "🤬": "tức giận", "🧚": "khác", "🧡": "thích thú", '🐶': ' khác ',
            '👎': ' buồn chán ', '😣': ' tức giận ', '✨': ' khác ', '❣': ' khác ', '☀': ' khác ',
            '♥': ' thích thú ', '🤩': ' thích thú ', 'like': ' thích thú ', '💌': ' thích thú ',
            '🤣': ' thích thú ', '🖤': ' thích thú ', '🤤': ' thích thú ', ':(': ' buồn chán ', '😢': ' buồn chán ',
            '❤': ' thích thú ', '😍': ' thích thú ', '😘': ' thích thú ', '😪': ' buồn chán ', '😊': ' thích thú ',
            '😁': ' thích thú ', '💖': ' thích thú ', '😟': ' buồn chán ', '😭': ' buồn chán ',
            '💯': ' thích thú ', '💗': ' thích thú ', '♡': ' thích thú ', '💜': ' thích thú ', '🤗': ' thích thú ',
            '^^': ' thích thú ', '😨': ' kinh tởm ', '☺': ' thích thú ', '💋': ' thích thú ', '👌': ' thích thú ',
            '😖': ' kinh tởm ', '😀': ' thích thú ', ':((': ' buồn chán ', '😡': ' tức giận ', '😠': ' tức giận ',
            '😒': ' buồn chán ', '🙂': ' thích thú ', '😏': ' kinh tởm ', '😝': ' thích thú ', '😄': ' thích thú ',
            '😙': ' thích thú ', '😤': ' tức giận ', '😎': ' thích thú ', '😆': ' thích thú ', '💚': ' thích thú ',
            '✌': ' thích thú ', '💕': ' thích thú ', '😞': ' buồn chán ', '😓': ' buồn chán ', '️🆗️': ' thích thú ',
            '😉': ' thích thú ', '😂': ' thích thú ', ':v': '  thích thú ', '=))': '  thích thú ', '😋': ' thích thú ',
            '💓': ' thích thú ', '😐': ' buồn chán ', ':3': ' thích thú ', '😫': ' buồn chán ', '😥': ' buồn chán ',
            '😃': ' thích thú ', '😬': 'sợ', '😌': ' buồn chán ', '💛': ' thích thú ', '🤝': ' thích thú ',
            '🎈': ' khác ',
            '😗': ' thích thú ', '🤔': ' kinh tởm ', '😑': ' tức giận ', '🔥': ' khác ', '🙏': ' thích thú ',
            '🆗': ' thích thú ', '😻': ' thích thú ', '💙': ' thích thú ', '💟': ' thích thú ',
            '😚': ' thích thú ', '❌': ' khác ', '👏': ' thích thú ', ';)': ' thích thú ', '<3': ' thích thú ',
            '🌝': ' thích thú ', '🌷': ' khác ', '🌸': ' thích thú ', '🌺': ' thích thú ',
            '🌼': ' khác ', '🍓': ' khác ', '🐅': ' khác ', '🐾': ' thích thú ', '👉': ' khác ',
            '💐': ' khác ', '💞': ' thích thú ', '💥': ' khác ', '💪': ' khác ',
            '💰': ' khác ', '😇': ' thích thú ', '😛': ' thích thú ', '😜': ' thích thú ',
            '🙃': ' thích thú ', '🤑': ' thích thú ', '🤪': ' thích thú ', '☹': ' buồn chán ', '💀': ' khác ',
            '😔': ' buồn chán ', '😧': ' buồn chán ', '😩': ' buồn chán ', '😰': ' buồn chán ', '😳': ' buồn chán ',
            '😵': ' bất ngờ ', '😶': ' buồn chán ', '🙁': ' buồn chán ', }

        for k, v in replace_list.items():
            string = string.replace(k, v)
        #     remove nốt những ký tự thừa thãi

        string = string.replace(u'"', u' ')
        string = string.replace(u'️', u'')
        kk = string.replace('🏻', '')

        kk = ViTokenizer.tokenize(kk)
        print(kk)
        dictionary = {}
        with open("data/xuly/tiengvietchuan.txt", "r", encoding='utf-8') as file:
            for line in file:
                key, value = line.strip().split(",")
                key1 = str(key).strip()
                value1 = str(value).strip()
                dictionary[key1] = value1

        def replace_string(text, dictionary):
            l = text.split(" ")
            for key in dictionary.keys():
                for item, va in enumerate(l):
                    if va == key:
                        l[item] = str(dictionary[key])
            result = " ".join(l)
            return result.strip()

        kk = replace_string(kk, dictionary)
        print(kk)
        def encode(input):
            from sklearn import preprocessing
            encoder = pickle.load(open('label_encode.pkl', 'rb'))
            x = []
            x.append(input)
            result = encoder.inverse_transform(x)
            return result

        data1 = open('vietnamese-stopwords.txt', 'r', encoding='utf-8')
        data = data1.read()

        # def stopword(x_test):
        #     data2 = []
        #     for line in x_test:
        #         item1 = ''
        #         for item in line.split():
        #             if item not in data:
        #                 item1 = str(item1) + str(item) + " "
        #         data2.append(item1)
        #
        #     return data2
        #
        # kk = stopword(kk)
        print(kk)
        x_data = []
        x1_data = []
        # tiền xử lí
        lines = gensim.utils.simple_preprocess(kk)
        lines = ' '.join(lines)
        lines = ViTokenizer.tokenize(lines)
        x_data.append(lines)
        x1_data.append(x_data)
        # chuyển đoạn test về dạng vector
        tfidf_vect = pickle.load(open('tfidf_vect.pkl', 'rb'))
        kk = tfidf_vect.transform(x_data)

        DecisionTree = pickle.load(open('DecisionTreeClassifier.pkl', 'rb'))
        LogisticRegression = pickle.load(open('LogisticRegression.pkl', 'rb'))
        MultinomialNB = pickle.load(open('MultinomialNB.pkl', 'rb'))
        RandomForestClassifier = pickle.load(open('RandomForestClassifier.pkl', 'rb'))
        SVC = pickle.load(open('SVC.pkl', 'rb'))
        # KẾT QUẢ DỰ ĐOÁN
        self.output.insert(END, "========DECISION TREE=============="+ "\n")
        test_predictions = DecisionTree.predict(kk)
        self.output.insert(END, encode(test_predictions))
        self.output.insert(END,"\n")
        self.output.insert(END, "\n")



        # self.output.insert(END, "=========NAIVE BAYES=============" + "\n")
        # test_predictions = MultinomialNB.predict(kk)
        # self.output.insert(END, encode(test_predictions))
        # self.output.insert(END,"\n")
        # self.output.insert(END, "\n")

        self.output.insert(END, "=========RandomForest=============" + "\n")
        test_predictions = RandomForestClassifier.predict(kk)
        self.output.insert(END, encode(test_predictions) )
        self.output.insert(END,"\n")
        self.output.insert(END, "\n")

        # self.output.insert(END, "=========SVM============="+ "\n")
        # test_predictions1 = SVC.predict(kk)
        # self.output.insert(END, encode(test_predictions1) )
        # self.output.insert(END,"\n")
        # self.output.insert(END, "\n")

        self.output.insert(END, "==========LMST============"+ "\n")
        test_predictions = LSTM.predict(kk).argmax(axis=-1)
        self.output.insert(END, encode(test_predictions) )
        self.output.insert(END,"\n")
        self.output.insert(END, "\n")

        self.output.insert(END, "===========CNN==========="+ "\n")
        test_predictions = CNN.predict(kk).argmax(axis=-1)
        self.output.insert(END, encode(test_predictions) )
        self.output.insert(END,"\n")
        self.output.insert(END, "\n")

        self.output.insert(END, "==========RNN============"+ "\n")
        test_predictions = RNN.predict(kk).argmax(axis=-1)
        self.output.insert(END, encode(test_predictions) )
        self.output.insert(END,"\n")
        self.output.insert(END, "\n")


    def Clear(self):
        self.update()
        self.output.delete("0.0", END)

    def __init__(self, master):
        super().__init__(master)
        self.NameGroup = cm.Label(self, text = "NHÓM 2", font = ("Time New Roman", 30))
        self.NameTecher1 = cm.Label(self, text = "GIẢNG VIÊN ", font = ("Time New Roman", 14))
        self.NameTecher2 = cm.Label(self, text = "NGUYỄN VĂN KIệT", font = ("Time New Roman", 14))
        self.NameSubject1 = cm.Label(self, text = "KHAI THÁC DỮ LIỆU", font = ("Time New Roman", 24))
        self.NameSubject2 = cm.Label(self, text = "TRUYỀN THÔNG XÃ HỘI", font = ("Time New Roman", 24))
        self.NameTT = cm.Label(self, text = "Câu đã chọn: ", font = ("Time New Roman", 12))
        self.NameTT1 = cm.Label(self, text = "HÃY VIẾT CÂU BẠN MUỐN THỬ ", font = ("Time New Roman", 12))
        self.Cau = cm.Entry(self, font = ("Time New Roman", 15))
        self.Clear=cm.Button(self, text = "Clear", command = self.Clear)
        self.xoutput = Scrollbar(self, orient=HORIZONTAL)
        self.output = sc.ScrolledText(self, wrap='none', xscrollcommand=self.xoutput.set)
        self.xoutput['command'] = self.output.xview
        self.Run = cm.Button(self, text = "RUN", command = self.RunMain)
        master.bind("<Configure>", self.placeGD)

    def placeGD (self, even):
        self.update()
        selfW = self.winfo_width()
        selfH = self.winfo_height()
        self.output.place(height = selfH - 300, width = selfW - 100, x = 50, y = 270)
        self.xoutput.place(height=20, width=selfW - 100, x=50, y=selfH - 35)
        self.NameTT1.place(height = 40, width = 400, x =290 , y = 160)
        self.Cau.place(height = 40 , width = 680, x = 50, y = 200)
        self.NameTT.place(height = 40, width = 400, x =50 , y = 230)
        self.Run.place(height = 40, width = 40, x = 735, y = 200)
        self.NameGroup.place(height = 100, width = 170, x = 30, y = 10)
        self.NameTecher1.place(height = 25, width = 230, x = 630, y = 5)
        self.NameTecher2.place(height = 20, width = 230, x = 580, y = 30)
        self.NameSubject1.place(height = 50, width = 350, x =250 , y = 50)
        self.NameSubject2.place(height = 40, width = 450, x =215 , y = 100)
        self.Clear.place(height = 40, width = 40, x =750 , y = 270)
GD = Tk()
GD.title("GROUP 4")
GD.geometry('800x800+0+0')
GD.configure(bg = 'red')
sky = Giaodien(GD)
sky.place(relwidth = 1, relheight = 1)
GD.mainloop()
