from tkinter import *
from tkinter import filedialog
# from tkinter import messagebox
from scipy.stats import zscore
import tkinter.ttk as cm
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import pandas as pd
import tkinter.scrolledtext as sc
import math
import csv
class Giaodien(Frame):
    def Chon(selt):
        selt.update()
        dataset = pd.read_csv(GD.filename, encoding='utf-8')
        # print(dataset2)
        # dataset1 = zscore(dataset2)
        # c = pd.DataFrame(dataset1)
        # c.to_csv('data/dataset2.csv', index=False, encoding='utf-8')
        # dataset = pd.read_csv('data/dataset2.csv', encoding='utf-8')
        # print(dataset)
        x = dataset.iloc[:, 1:-1]
        y = dataset.iloc[:, -1]
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
        # ----------------------luu file train test--------------------------
        df = pd.DataFrame(x_train)
        df.to_csv('trainfile.csv', index=False, encoding='utf-8')
        df = pd.DataFrame(x_test)
        df.to_csv('testfile.csv', index=False, encoding='utf-8')
        if selt.BTTToan.get() == "Decision Tree":
            tree = DecisionTreeClassifier(criterion='entropy').fit(x_train, y_train)
            y_pred = tree.predict(x_test)
            print('Accuracy của Decision Tree là:', round(accuracy_score(y_test, y_pred) * 100), "%")
            t=round(accuracy_score(y_test, y_pred) * 100)
            str(t)
            print(classification_report(y_test, y_pred))
            print(confusion_matrix(y_test, y_pred))
            r = "Decision Tree"
            selt.output.insert(END, "                     Accuracy của Decision Tree là: "+ str(t) +"\n"+ str(classification_report(y_test, y_pred)) +"\n"+str(confusion_matrix(y_test, y_pred))+ "\n")
            print(dataset)
        elif selt.BTTToan.get() == "Naive Bayes":
            from sklearn.naive_bayes import GaussianNB
            classifier = GaussianNB()
            classifier.fit(x_train, y_train)
            y_pred = classifier.predict(x_test)
            print('Accuracy của Naive Bayes là:', accuracy_score(y_test, y_pred) * 100, "%")
            print(classification_report(y_test, y_pred))
            print(confusion_matrix(y_test, y_pred))
            t=round(accuracy_score(y_test, y_pred) * 100)
            selt.output.insert(END, "                     Accuracy của Naive Bayes là: "+ str(t) +"\n"+ str(classification_report(y_test, y_pred)) +"\n"+str(confusion_matrix(y_test, y_pred))+ "\n")
            r = "Naive Bayes"
        elif selt.BTTToan.get() == "SVM":
            from sklearn.svm import SVC
            svm_model_linear = SVC(kernel='linear', C=1).fit(x_train, y_train)
            y_pred = svm_model_linear.predict(x_test)
            print("Accuracy của SVM: ", accuracy_score(y_test, y_pred) * 100, "%")
            print(classification_report(y_test, y_pred))
            print(confusion_matrix(y_test, y_pred))
            t = round(accuracy_score(y_test, y_pred) * 100)
            selt.output.insert(END, "                     Accuracy của SVM là: " + str(t) + "\n" + str(classification_report(y_test, y_pred)) + "\n" + str(confusion_matrix(y_test, y_pred))+ "\n")
            r = "SVM"
        elif selt.BTTToan.get() == "KNN":
            from sklearn.preprocessing import StandardScaler
            from sklearn.neighbors import KNeighborsClassifier
            scaler = StandardScaler()
            scaler.fit(x_train)
            X_train = scaler.transform(x_train)
            X_test = scaler.transform(x_test)
            classifier = KNeighborsClassifier(n_neighbors=5)
            classifier.fit(X_train, y_train)
            y_pred_1 = classifier.predict(X_test)
            print('Accuracy của kNN là:', round(accuracy_score(y_test, y_pred_1) * 100, 2), "%")
            print(classification_report(y_test, y_pred_1))
            print(confusion_matrix(y_test, y_pred_1))
            t = round(accuracy_score(y_test, y_pred_1) * 100)
            selt.output.insert(END, "                     Accuracy của KNN là: " + str(t) + "\n" + str(classification_report(y_test, y_pred_1)) + "\n" + str(confusion_matrix(y_test, y_pred_1))+ "\n")
            r = "KNN"
        elif selt.BTTToan.get() == "Random Forest":
            from sklearn.ensemble import RandomForestClassifier
            clf = RandomForestClassifier(n_estimators=100)
            clf.fit(x_train, y_train)
            y_pred = clf.predict(x_test)
            print("Accuracy của Random Forest: ", accuracy_score(y_test, y_pred) * 100, "%")
            print(classification_report(y_test, y_pred))
            print(confusion_matrix(y_test, y_pred))
            t = round(accuracy_score(y_test, y_pred) * 100)
            selt.output.insert(END, "                     Accuracy của Random Forest là: " + str(t) + "\n" + str(classification_report(y_test, y_pred)) + "\n" + str(confusion_matrix(y_test, y_pred))+ "\n")

            r = "Sklearn"
        else:
            r = ""
        selt.NameTT['text'] = "Thuật toán: " + r
    def Clear(self):
        self.update()
        self.output.delete("0.0", END)
    def Open(self):
        self.update()
        GD.filename = filedialog.askopenfilename()
        self.Namefile['text'] = "File đã chọn: " + GD.filename
        dataset1 = pd.read_csv(GD.filename, encoding='utf-8')
        self.output.insert(END, dataset1)

    def __init__(self, master):
        super().__init__(master)
        self.NameGroup = cm.Label(self, text = "NHÓM 4", font = ("Time New Roman", 30))
        self.NameTecher1 = cm.Label(self, text = "GIẢNG VIÊN ", font = ("Time New Roman", 14))
        self.NameTecher2 = cm.Label(self, text = "NGUYỄN THỊ ANH THƯ", font = ("Time New Roman", 14))
        self.NameSubject1 = cm.Label(self, text = "KHAI THÁC DỮ LIỆU", font = ("Time New Roman", 24))
        self.NameSubject2 = cm.Label(self, text = "ỨNG DỤNG", font = ("Time New Roman", 24))
        self.Namefile = cm.Label(self, text = "Hãy chọn dữ liệu", font = ("Time New Roman", 12))
        self.NameTT = cm.Label(self, text = "Hãy chọn thuật toán", font = ("Time New Roman", 12))
        self.BTfile=cm.Button(self, text = "Chọn file", command = self.Open)
        self.BTTToan = cm.Combobox(self)
        self.BTTToan['value'] = ("Decision Tree", "Naive Bayes", "Random Forest", "SVM", "KNN")
        self.Clear=cm.Button(self, text = "Clear data", command = self.Clear)
        self.xoutput = Scrollbar(self, orient = HORIZONTAL)
        self.output = sc.ScrolledText(self, wrap = 'none', xscrollcommand = self.xoutput.set)
        self.xoutput['command']= self.output.xview
        self.Run = cm.Button(self, text = "RUN", command = self.Chon)
        master.bind("<Configure>", self.placeGD)

    def placeGD (self, even):
        self.update()
        selfW = self.winfo_width()
        selfH = self.winfo_height()
        self.output.place(height = selfH - 230, width = selfW - 200, x = 195, y = 195)
        self.xoutput.place(height = 20, width = selfW - 210, x = 195, y = selfH - 35)
        self.BTfile.place(height = 25, width = 70, x = 65, y = 230)
        self.BTTToan.place(height = 30 , width = 180, x = 10, y = 280)
        self.Run.place(height = 100, width = 100, x = 55, y = 380)
        self.NameGroup.place(height = 100, width = 170, x = 30, y = 10)
        self.NameTecher1.place(height = 25, width = 230, x = 630, y = 5)
        self.NameTecher2.place(height = 20, width = 230, x = 580, y = 30)
        self.NameSubject1.place(height = 50, width = 350, x =270 , y = 50)
        self.NameSubject2.place(height = 40, width = 350, x =320 , y = 100)
        self.Clear.place(height = 40, width = 100, x =55 , y = 520)
        self.NameTT.place(height = 40, width = 400, x =0 , y = 310)
        self.Namefile.place(height = 40, width = 600, x =0 , y = 160)
GD = Tk()
GD.title("GROUP 4")
GD.geometry('800x600+0+0')
GD.configure(bg = 'red')
sky = Giaodien(GD)
sky.place(relwidth = 1, relheight = 1)
GD.mainloop()
